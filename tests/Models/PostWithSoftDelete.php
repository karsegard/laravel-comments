<?php

namespace KDA\Tests\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;

class PostWithSoftDelete extends Model 
{
   
    use HasFactory;
    use SoftDeletes;

    protected $table="posts";
    protected $fillable = [
        'title'
    ];

    public function slugCollectionName(){
        return 'Posts';
    }

    public function getSluggableAttribute(){
        return $this->title;
    }
    protected static function newFactory()
    {
        return  \KDA\Tests\Database\Factories\PostWithSoftDeleteFactory::new();
    }
}
