<?php

namespace KDA\Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use KDA\Laravel\Comments\Facades\CommentManager;
use KDA\Laravel\Comments\Models\Comment;
use KDA\Laravel\Comments\ServiceProvider;
use KDA\Tests\Models\Post;
use KDA\Tests\Models\PostWithSoftDelete;
use KDA\Tests\Models\User;
use KDA\Tests\Models\UserWithSoftDelete;
use KDA\Tests\TestCase;

class ModelTest extends TestCase
{
  use RefreshDatabase;


  /** @test */
  function comment_created()
  {
    $o = Comment::factory()->create([]);
    $this->assertNotNull($o);

  }


  /** @test */
  function comment_created_with_array()
  {
    $o = Comment::factory()->create(['comment'=>['title'=>'test','comment'=>'aloha world']]);
    $this->assertNotNull($o);
    
  }

  /** @test */
  function register_model(){
    CommentManager::registerModel(Post::class);
    $post = Post::factory()->create();
    $comment = Comment::factory()->create([
      'model_id'=>$post->getKey(),
      'model_type'=>get_class($post)
    ]);

    $this->assertNotNull($post->comments);
    $this->assertEquals(1,$post->comments->count());
    $this->assertNotNull($comment->commentable);
    $this->assertNull($comment->author);

  }


  /** @test */
  function register_author_model(){
    CommentManager::registerAuthorModel(User::class);
    $user = User::factory()->create();
    $post = Post::factory()->create();
    $comment = Comment::factory()->create([
      'author_id'=>$user->getKey(),
      'author_type'=>get_class($user),
      'model_id'=>$post->getKey(),
      'model_type'=>get_class($post)
    ]);

    $this->assertNotNull($user->comments);
    $this->assertEquals(1,$user->comments->count());

    $this->assertNotNull($comment->commentable);
    $this->assertNotNull($comment->author);


  }


  /** @test */
  function comment_delete(){
    CommentManager::registerModel(Post::class);
    $post = Post::factory()->create();
    $comment = Comment::factory()->create([
      'model_id'=>$post->getKey(),
      'model_type'=>get_class($post)
    ]);


    $this->assertNotNull($comment->commentable);
    $this->assertNotNull($post->comments);


    $post->delete();

    $this->assertDatabaseCount(ServiceProvider::getTableName('comments'),0);

  }
  
  /** @test */
  function author_delete_doesnt_delete_if_not_chained(){
    CommentManager::registerModel(Post::class);
    CommentManager::registerAuthorModel(User::class,false);
    $user = User::factory()->create();
    $post = Post::factory()->create();
    $comment = Comment::factory()->create([
      'author_id'=>$user->getKey(),
      'author_type'=>get_class($user),
      'model_id'=>$post->getKey(),
      'model_type'=>get_class($post)
    ]);

    $this->assertNotNull($comment->commentable);
    $this->assertNotNull($post->comments);


    $user->delete();

    $this->assertDatabaseCount(ServiceProvider::getTableName('comments'),1);

  }


  /** @test */
  function author_delete_deletes_comment_if_chained(){
    CommentManager::registerModel(Post::class);
    CommentManager::registerAuthorModel(User::class);
    $user = User::factory()->create();
    $post = Post::factory()->create();
    $comment = Comment::factory()->create([
      'author_id'=>$user->getKey(),
      'author_type'=>get_class($user),
      'model_id'=>$post->getKey(),
      'model_type'=>get_class($post)
    ]);

    $this->assertNotNull($comment->commentable);
    $this->assertNotNull($post->comments);


    $user->delete();

    $this->assertDatabaseCount(ServiceProvider::getTableName('comments'),0);

  }
  


  /** @test */
  function comment_not_deleted_when_model_is_soft_deletable(){
    CommentManager::registerModel(PostWithSoftDelete::class);
    $post = PostWithSoftDelete::factory()->create();
    $comment = Comment::factory()->create([
      'model_id'=>$post->getKey(),
      'model_type'=>get_class($post)
    ]);


    $this->assertNotNull($comment->commentable);
    $this->assertNotNull($post->comments);

    $post->delete();

    $this->assertDatabaseCount(ServiceProvider::getTableName('comments'),1);
    
  }
  


  /** @test */
  function comment_not_deleted_when_author_is_soft_deletable(){
    CommentManager::registerModel(PostWithSoftDelete::class);
    CommentManager::registerAuthorModel(UserWithSoftDelete::class);
    $user = UserWithSoftDelete::factory()->create();
    $post = PostWithSoftDelete::factory()->create();
    $comment = Comment::factory()->create([
      'author_id'=>$user->getKey(),
      'author_type'=>get_class($user),
      'model_id'=>$post->getKey(),
      'model_type'=>get_class($post)
    ]);

    $this->assertNotNull($comment->commentable);
    $this->assertNotNull($post->comments);


    $user->delete();

    $this->assertDatabaseCount(ServiceProvider::getTableName('comments'),1);
    $this->assertTrue(Comment::first()->author_deleted);

    $user->restore();
    $this->assertDatabaseCount(ServiceProvider::getTableName('comments'),1);
    $this->assertFalse(Comment::first()->author_deleted);
  }
  
}