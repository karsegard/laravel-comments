<?php

namespace KDA\Tests\Database\Factories;

use KDA\Tests\Models\UserWithSoftDelete;
use Illuminate\Database\Eloquent\Factories\Factory;

class UserWithSoftDeleteFactory extends Factory
{
    protected $model = UserWithSoftDelete::class;

    public function definition()
    {
        return [
            'name' => $this->faker->name(),
            'email'=>$this->faker->email(),
            'password'=>$this->faker->word()
        ];
    }
}
