<?php

namespace KDA\Tests\Database\Factories;

use KDA\Tests\Models\PostWithSoftDelete;
use Illuminate\Database\Eloquent\Factories\Factory;

class PostWithSoftDeleteFactory extends Factory
{
    protected $model = PostWithSoftDelete::class;

    public function definition()
    {
        return [
            'title' => $this->faker->sentence(4),
        ];
    }
}
