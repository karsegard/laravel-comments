<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use KDA\Laravel\Comments\ServiceProvider;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::create(ServiceProvider::getTableName('comments'), function (Blueprint $table) {
            $table->id();
            $table->numericMorphs('model');
            $table->longText('comment');
            $table->nullableNumericMorphs('author');     
            $table->boolean('author_deleted')->default(false);      
            $table->timestamps();
        });

        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists(ServiceProvider::getTableName('comments'));
     
        Schema::enableForeignKeyConstraints();
    }
};