<?php

namespace KDA\Laravel\Comments\Database\Factories;

use KDA\Laravel\Comments\Models\Comment;
use Illuminate\Database\Eloquent\Factories\Factory;
use KDA\Tests\Models\Post;

class CommentFactory extends Factory
{
    protected $model = Comment::class;

    public function definition()
    {
        $post = Post::factory()->create();
        return [
            'model_type'=>get_class($post),
            'model_id'=>$post->getKey(),
            'comment'=>$this->faker->text(200)
        ];
    }
}
