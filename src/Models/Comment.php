<?php

namespace KDA\Laravel\Comments\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use KDA\Laravel\Comments\ServiceProvider;

class Comment extends Model
{
    use HasFactory;

    protected $fillable = [
        'comment',
        'author_id',
        'author_type',
        'model_id',
        'model_type'
    ];

    protected $appends = [];

    protected $casts = [
        'author_deleted' => 'boolean',
        'comment'=>'json'
    ];

    public static function boot(): void
    {
        parent::boot();
        static::creating(
            function (Model $model) {
                $author = auth()->user();
               /* if ($author) {
                    $model->author_type = get_class($author);
                    $model->author_id = $author->getKey();
                }*/
                $model->author()->associate($author);
            }
        );
    }


    public function getTable()
    {
        return ServiceProvider::getTableName('comments');
    }

    protected static function newFactory()
    {
        return  \KDA\Laravel\Comments\Database\Factories\CommentFactory::new();
    }

    public function commentable()
    {
        return $this->morphTo('model');
    }

    public function author()
    {
        return $this->morphTo('author');
    }

    public function scopeForModel($q, $model)
    {
      //  return $q->where('model_type', get_class($model))->where('model_id', $model->getKey());
      return $q->whereMorphedTo('model',$model);
    }


    public function scopeForAuthor($q, $model)
    {
        //return $q->where('author_type', get_class($model))->where('author_id', $model->getKey());
        return $q->whereMorphedTo('author',$model);
    }

   
}
