<?php

namespace KDA\Laravel\Comments\Models\Traits;

use KDA\Laravel\Comments\Facades\CommentManager;
use KDA\Laravel\Comments\Models\Comment;

trait HasComment
{

    public static function bootHasComment(): void
    {
        static::creating(function ($model)
        {
        });
        static::updating(function ($model)
        {
        });
    }


    public function comments()
    {
        return $this->morphMany(Comment::class, 'model');
    }

    public function addComment($comment){
        CommentManager::addComment($this,$comment);
    }
/*
    public function has_one_of_many_polymorphic()
    {
        return $this->morphOne(Slug::class, 'sluggable')->latestOfMany();
    }

    public function scopeWithNoSlug($query)
    {
        return $query->whereDoesntHave('slugs');
     }*/
}
