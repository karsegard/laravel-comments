<?php

namespace KDA\Laravel\Comments;

use KDA\Laravel\PackageServiceProvider;
use KDA\Laravel\Traits\HasCommands;
//use Illuminate\Support\Facades\Blade;
use KDA\Laravel\Comments\Facades\CommentManager as Facade;
use KDA\Laravel\Comments\CommentManager as Library;
use KDA\Laravel\Traits\HasConfig;
use KDA\Laravel\Traits\HasConfigurableTableNames;
use KDA\Laravel\Traits\HasLoadableMigration;
use KDA\Laravel\Traits\HasDumps;

class ServiceProvider extends PackageServiceProvider
{
    use HasCommands;
    use HasConfig;
    use HasLoadableMigration;
    use HasConfigurableTableNames;
    use HasDumps;
    protected static $tables_config_key = 'kda.laravel-comments.tables';

    protected $packageName = 'laravel-comments';
    protected $dumps= [
        'kda_comments',
    ];
    protected function packageBaseDir()
    {
        return dirname(__DIR__, 1);
    }
    // trait \KDA\Laravel\Traits\HasConfig; 
    //    registers config file as 
    //      [file_relative_to_config_dir => namespace]
    protected $configDir = 'config';
    protected $configs = [
        'kda/comments.php'  => 'kda.laravel-comments'
    ];
    //  trait \KDA\Laravel\Traits\HasLoadableMigration
    //  registers loadable and not published migrations
    // protected $migrationDir = 'database/migrations';
    public function register()
    {
        parent::register();
        $this->app->singleton(Facade::class, function () {
            return new Library();
        });
    }
    /**
     * called after the trait were registered
     */
    public function postRegister()
    {
    }
    //called after the trait were booted
    protected function bootSelf()
    {
    }
}
