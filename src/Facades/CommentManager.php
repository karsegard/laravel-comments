<?php

namespace KDA\Laravel\Comments\Facades;

use Illuminate\Support\Facades\Facade;

class CommentManager extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return static::class;
    }
}
