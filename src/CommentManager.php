<?php

namespace KDA\Laravel\Comments;

use Illuminate\Database\Eloquent\Model;
use KDA\Laravel\Comments\Models\Comment;

//use Illuminate\Support\Facades\Blade;
class CommentManager
{

    public function addComment(Model $model,$comment,$anonymous = false){
       /*Comment::create([
            'model_id'=>$model->getKey(),
            'model_type'=>get_class($model),
            'comment'=>$comment
        ]);*/
        $comment = new Comment(['comment'=>$comment]);
        $comment->model()->associate($model);
        $comment->save();
    }
    public function deleteCommentsForModel(Model $model)
    {
        if ($this->isSoftDeleting($model)) {
           // Comment::forAuthor($model)->update(['model_deleted' => true]);
            return;
        }
        Comment::forModel($model)->delete();
    }

    public function deleteCommentsFoAuthor(Model $model,$chainDelete=false)
    {
        if ($this->isSoftDeleting($model)) {

            Comment::forAuthor($model)->update(['author_deleted' => true]);
            return;
        }
        if($chainDelete){
             Comment::forAuthor($model)->delete();
        }else{
            Comment::forAuthor($model)->update(['author_id'=>null,'author_type'=>null]);
        }
    }

    public function restoreCommentsFoAuthor(Model $model,$chainDelete=false)
    {
        Comment::forAuthor($model)->update(['author_deleted' => false]);
    }

    public function usesSoftDeleting($model): bool
    {
        return in_array('Illuminate\Database\Eloquent\SoftDeletes', class_uses($model));
    }
    public function isSoftDeleting($model): bool
    {
        return $this->usesSoftDeleting($model) && !$model->isForceDeleting();
    }

    public function registerModel(string $model,  $withHooks = true)
    {
        $model::resolveRelationUsing('comments', function ($model) {
            return $model->morphMany(Comment::class, 'model');
        });

        if ($withHooks) {
            $model::deleted(function ($model) {
                $this->deleteCommentsForModel($model);
            });
        /*    if ($this->usesSoftDeleting($model)) {
                $model::restored(function ($model) {
                    $this->restoreCommentsForModel($model);
                });
            }*/
        }
    }

    public function registerAuthorModel(string $model,  $withHooks = true, $chainDelete = true)
    {
        $model::resolveRelationUsing('comments', function ($model) {
            return $model->morphMany(Comment::class, 'author');
        });

        if ($withHooks) {
            $model::deleted(function ($model) use($chainDelete){
                $this->deleteCommentsFoAuthor($model,$chainDelete);
            });
            if ($this->usesSoftDeleting($model)) {
                $model::restored(function ($model) {
                    $this->restoreCommentsFoAuthor($model);
                });
            }
        }
    }




    /*
    public function registerReverseCommentableRelationship(string $reverseModel, string $name){
        Comment::resolveRelationUsing($name,function($model) use ($reverseModel){
            return $model->morphedByMany($reverseModel, 'model',ServiceProvider::getTableName('comments'),'id');
        });
    }

    public function registerReverseAuthorRelationship(string $reverseModel, string $name){
        Comment::resolveRelationUsing($name,function($model) use ($reverseModel){
            return $model->morphedByMany($reverseModel, 'author',ServiceProvider::getTableName('comments'),'id');
        });
    }*/
}
